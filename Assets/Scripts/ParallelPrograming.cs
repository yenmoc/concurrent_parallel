﻿using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using UnityEngine;
using UnityModule.Attributes;

public class ParallelPrograming : MonoBehaviour
{
    /*
     *Trước hết, trong một chương trình không đa luồng, việc xử lý được thực hiện tuần tự từ đầu với một lõi CPU.
     * Tất nhiên, với các CPU lõi đơn chỉ có một lõi CPU,
     * Ngay cả đối với CPU đa lõi, việc xử lý được thực hiện chỉ bằng một CPU. Không song song.
     * 
     */


    /*
     * CPU 1 : WorkA -> WorkB -> WorkC
     *
     *
     * Nếu bạn viết một chương trình đa luồng ở đây, CPU đa lõi sẽ thực hiện xử lý khác nhau trong lõi CPU khác nhau cho mỗi luồng.
     * 
     * CPU 1 : WorkA
     * CPU 2 : WorkB
     * CPU 3 : WorkC
     *
     *
     * Trong CPU lõi đơn, mặc dù việc xử lý được thực hiện bởi một lõi CPU,
     * việc xử lý song song giả sử được thực hiện do quá trình xử lý được thực hiện từng chút một trong khi chuyển đổi từng luồng ở tốc độ cao.
     *
     * CPU 1 : Work a -> Work b -> Work c -> Work a -> Work b -> Work c -> Work a -> Work b -> Work c
     * 
     */


    /* ----------------------------  #1 Giảm thời gian xử lý  --------------------------------
     *
     * Một trong những lợi ích của đa luồng là : CPU đa lõi giúp giảm thời gian xử lý.
     * Như đã thấy rõ trong hình bên dưới, có thể giảm toàn bộ thời gian xử lý vì việc xử lý có thể được thực hiện trên các lõi CPU không được sử dụng trong các CPU đa lõi.
     *
     * CPU 1 : WorkA
     * CPU 2 : WorkB
     * CPU 3 : WorkC
     *
     *
     *
     * Tuy nhiên, CPU lõi đơn cần có thời gian cho quá trình chuyển đổi luồng và thời gian xử lý tổng thể sẽ lâu hơn.
     *
     * CPU 1 : WorkA -> WorkB -> WorkC
     * CPU 1 : Work a -(chuyển đổi luồng)> Work b -> Work c -> Work a -> Work b -> Work c -> Work a -> Work b -> Work c
     */


    /* ----------------------------  #2 Ngăn ngừa đóng băng màn hình  --------------------------------
     * Đa luồng cũng có tác dụng ngăn chặn đóng băng màn hình.
     *
     * Ví dụ, thực hiện điều này trong luồng chính khi xử lý một tính toán rất tốn thời gian,
     * Màn hình đóng băng vì các quy trình khác không thể được thực hiện cho đến khi tính toán được hoàn thành.
     * Nếu quá trình tính toán này được phát hành cho một luồng khác ở đây, thì luồng chính có thể thực hiện xử lý khác trong thời gian đó.
     * Nói cách khác, màn hình không bị đóng băng.
     * 
     */

    /* ----------------------------  Example  --------------------------------
     *
     *
     * ProcessHeavyTasksParallel is fastest more than ProcessHeavyTasksSequential
     * 
     */


    private const long LOOP_COUNT = 6;

    [Button()]
    private async Task ProcessHeavyTasksSequential()
    {
        var stopwatch = new Stopwatch();
        stopwatch.Start();

        for (var i = 0; i < LOOP_COUNT; i++)
        {
            await HeavyTaskAsync();
        }

        UnityEngine.Debug.Log(stopwatch.ElapsedMilliseconds);
        stopwatch.Stop();
    }

    [Button()]
    private async Task ProcessHeavyTasksParallel()
    {
        var stopwatch = new Stopwatch();
        stopwatch.Start();

        var tasks = new List<Task>();
        for (var i = 0; i < LOOP_COUNT; i++)
        {
            tasks.Add(HeavyTaskAsync());
        }

        await Task.WhenAll(tasks);

        UnityEngine.Debug.Log(stopwatch.ElapsedMilliseconds);
        stopwatch.Stop();
    }

    private static async Task HeavyTaskAsync()
    {
        await Task.Run(() =>
        {
            for (var i = 0; i < 1000000; i++)
            {
                var example = Mathf.Pow(2, 10);
            }
        });
    }


    /* ----------------------------  Xử lý CPU có hiệu quả  --------------------------------
     *
     *
     * Khi thực hiện lập trình đa luồng, đa luồng hoạt động hiệu quả Có vẻ như bạn phải nhận thức được rằng đó là xử lý CPU.
     *
     * Ví dụ, vì tệp IO được xử lý bởi đĩa, tôi nghĩ rằng lợi ích của việc song song hóa không thể thu được nhiều như vậy.
     * 
     */

    private const string FILE_PATH = "";
    private async Task ReadFilesSequential()
    {
        var data = File.ReadAllBytes(FILE_PATH);

        var stopwatch = new Stopwatch();
        stopwatch.Start();

        for (var i = 0; i < LOOP_COUNT; i++)
        {
            await ReadFileAsync(FILE_PATH);
        }

        UnityEngine.Debug.Log(stopwatch.Elapsed);
        stopwatch.Stop();
    }

    private async Task ReadFilesParallel()
    {
        var data = File.ReadAllBytes(FILE_PATH);
        var stopwatch = new Stopwatch();
        stopwatch.Start();

        var tasks = new List<Task>();
        for (var i = 0; i < LOOP_COUNT; i++)
        {
            tasks.Add(ReadFileAsync(FILE_PATH));
        }

        await Task.WhenAll(tasks);

        UnityEngine.Debug.Log(stopwatch.Elapsed);
        stopwatch.Stop();
    }

    private async Task ReadFileAsync(string filePath)
    {
        using (var fs = new FileStream(FILE_PATH, FileMode.Open, FileAccess.Read))
        {
            try
            {
                var bs = new byte[fs.Length];
                await fs.ReadAsync(bs, 0, bs.Length);
            }
            catch
            {
                // ignored
            }
            finally
            {
                if (fs != null)
                {
                    fs.Close();
                }
            }
        }
    }
}