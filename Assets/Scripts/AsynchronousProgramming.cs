﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsynchronousProgramming : MonoBehaviour
{
    /*
     * we must take a closer look at how the operating system communicates with applications.
     * Whenever the OS needs to notify the application of something, be it the user clicking a button or wanting to close the application;
     * it sends a message with all the information describing the action to the application. These messages are stored in a queue,
     * waiting for the application to process them and react accordingly.
     *
     *
     * Each application with a graphical user interface (GUI) has a main message loop which continuously checks the contents of this queue.
     * If there are any unprocessed messages in the queue, it takes out the first one and processes it. In a higher-level language such as C #,
     * this usually results in invoking a corresponding event handler. The code in the event handler executes synchronously. Until it completes,
     * none of the other messages in the queue are processed. If it takes too long, the application will appear to stop responding to user interaction.
     *
     *
     * Asynchronous programming using async and await keywords provides a simple way to avoid this problem with minimal code changes
     *
     *
     */


/*
    private void OnRequestDownload(object sender, RoutedEventArgs e)
    {
        var request = HttpWebRequest.Create(_requestedUri);
        var response = request.GetResponse();
        // process the response
    }

    //Here is an asynchronous version of the same code:
    private async void OnRequestDownload(object sender, RoutedEventArgs e)
    {
        var request = HttpWebRequest.Create(_requestedUri);
        var response = await request.GetResponseAsync();
        // process the response
    }
    
    
*/


/*
 * we need to call all asynchronous methods using the await keyword
 * Call await DoSomeLengthyStuffAsync();                                 Not Call DoSomeLengthyStuffAsync(); only
 *
 * 
 */

}