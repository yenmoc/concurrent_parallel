# concurrent_parallel

## What

* 

## Requirement

* 

## Install

```shell
yarn add "git+ssh://git@gitlab.com:yenmoc/concurrent_parallel"
```

## Usage

```csharp
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using UnityEngine;
using UnityModule.Attributes;

public class ParallelPrograming : MonoBehaviour
{
    /*
     *Trước hết, trong một chương trình không đa luồng, việc xử lý được thực hiện tuần tự từ đầu với một lõi CPU.
     * Tất nhiên, với các CPU lõi đơn chỉ có một lõi CPU,
     * Ngay cả đối với CPU đa lõi, việc xử lý được thực hiện chỉ bằng một CPU. Không song song.
     * 
     */


    /*
     * CPU 1 : WorkA -> WorkB -> WorkC
     *
     *
     * Nếu bạn viết một chương trình đa luồng ở đây, CPU đa lõi sẽ thực hiện xử lý khác nhau trong lõi CPU khác nhau cho mỗi luồng.
     * 
     * CPU 1 : WorkA
     * CPU 2 : WorkB
     * CPU 3 : WorkC
     *
     *
     * Trong CPU lõi đơn, mặc dù việc xử lý được thực hiện bởi một lõi CPU,
     * việc xử lý song song giả sử được thực hiện do quá trình xử lý được thực hiện từng chút một trong khi chuyển đổi từng luồng ở tốc độ cao.
     *
     * CPU 1 : Work a -> Work b -> Work c -> Work a -> Work b -> Work c -> Work a -> Work b -> Work c
     * 
     */


    /* ----------------------------  #1 Giảm thời gian xử lý  --------------------------------
     *
     * Một trong những lợi ích của đa luồng là : CPU đa lõi giúp giảm thời gian xử lý.
     * Như đã thấy rõ trong hình bên dưới, có thể giảm toàn bộ thời gian xử lý vì việc xử lý có thể được thực hiện trên các lõi CPU không được sử dụng trong các CPU đa lõi.
     *
     * CPU 1 : WorkA
     * CPU 2 : WorkB
     * CPU 3 : WorkC
     *
     *
     *
     * Tuy nhiên, CPU lõi đơn cần có thời gian cho quá trình chuyển đổi luồng và thời gian xử lý tổng thể sẽ lâu hơn.
     *
     * CPU 1 : WorkA -> WorkB -> WorkC
     * CPU 1 : Work a -(chuyển đổi luồng)> Work b -> Work c -> Work a -> Work b -> Work c -> Work a -> Work b -> Work c
     */


    /* ----------------------------  #2 Ngăn ngừa đóng băng màn hình  --------------------------------
     * Đa luồng cũng có tác dụng ngăn chặn đóng băng màn hình.
     *
     * Ví dụ, thực hiện điều này trong luồng chính khi xử lý một tính toán rất tốn thời gian,
     * Màn hình đóng băng vì các quy trình khác không thể được thực hiện cho đến khi tính toán được hoàn thành.
     * Nếu quá trình tính toán này được phát hành cho một luồng khác ở đây, thì luồng chính có thể thực hiện xử lý khác trong thời gian đó.
     * Nói cách khác, màn hình không bị đóng băng.
     * 
     */

    /* ----------------------------  Example  --------------------------------
     *
     *
     * ProcessHeavyTasksParallel is fastest more than ProcessHeavyTasksSequential
     * 
     */


    private const long LOOP_COUNT = 6;

    [Button()]
    private async Task ProcessHeavyTasksSequential()
    {
        var stopwatch = new Stopwatch();
        stopwatch.Start();

        for (var i = 0; i < LOOP_COUNT; i++)
        {
            await HeavyTaskAsync();
        }

        UnityEngine.Debug.Log(stopwatch.ElapsedMilliseconds);
        stopwatch.Stop();
    }

    [Button()]
    private async Task ProcessHeavyTasksParallel()
    {
        var stopwatch = new Stopwatch();
        stopwatch.Start();

        var tasks = new List<Task>();
        for (var i = 0; i < LOOP_COUNT; i++)
        {
            tasks.Add(HeavyTaskAsync());
        }

        await Task.WhenAll(tasks);

        UnityEngine.Debug.Log(stopwatch.ElapsedMilliseconds);
        stopwatch.Stop();
    }

    private static async Task HeavyTaskAsync()
    {
        await Task.Run(() =>
        {
            for (var i = 0; i < 1000000; i++)
            {
                var example = Mathf.Pow(2, 10);
            }
        });
    }


    /* ----------------------------  Xử lý CPU có hiệu quả  --------------------------------
     *
     *
     * Khi thực hiện lập trình đa luồng, đa luồng hoạt động hiệu quả Có vẻ như bạn phải nhận thức được rằng đó là xử lý CPU.
     *
     * Ví dụ, vì tệp IO được xử lý bởi đĩa, tôi nghĩ rằng lợi ích của việc song song hóa không thể thu được nhiều như vậy.
     * 
     */

    private const string FILE_PATH = "";
    private async Task ReadFilesSequential()
    {
        var data = File.ReadAllBytes(FILE_PATH);

        var stopwatch = new Stopwatch();
        stopwatch.Start();

        for (var i = 0; i < LOOP_COUNT; i++)
        {
            await ReadFileAsync(FILE_PATH);
        }

        UnityEngine.Debug.Log(stopwatch.Elapsed);
        stopwatch.Stop();
    }

    private async Task ReadFilesParallel()
    {
        var data = File.ReadAllBytes(FILE_PATH);
        var stopwatch = new Stopwatch();
        stopwatch.Start();

        var tasks = new List<Task>();
        for (var i = 0; i < LOOP_COUNT; i++)
        {
            tasks.Add(ReadFileAsync(FILE_PATH));
        }

        await Task.WhenAll(tasks);

        UnityEngine.Debug.Log(stopwatch.Elapsed);
        stopwatch.Stop();
    }

    private async Task ReadFileAsync(string filePath)
    {
        using (var fs = new FileStream(FILE_PATH, FileMode.Open, FileAccess.Read))
        {
            try
            {
                var bs = new byte[fs.Length];
                await fs.ReadAsync(bs, 0, bs.Length);
            }
            catch
            {
                // ignored
            }
            finally
            {
                if (fs != null)
                {
                    fs.Close();
                }
            }
        }
    }
}
```


*Concurent Programing
```csharp
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;
using UnityModule.Attributes;
using Debug = UnityEngine.Debug;

public class ConccurentPrograming : MonoBehaviour
{
    /*
     *Đồng thời là khi hai hoặc nhiều nhiệm vụ chạy có thể bắt đầu , chạy và hoàn thành trong khoảng thời gian chồng chéo.
     * Nó không nhất thiết là tất cả đều được chạy ngay lập tức. Ví dụ đa nhiệm trên một bộ xử lý đơn lõi.
     *
     * 
     *Song song là khi các nhiệm vụ chạy cùng một lúc. Ví dụ nhiều nhiệm vụ chạy cùng lúc trên một bộ xử lý đa lõi.
     * 
     */


    #region Example cancle task

    private readonly CancellationTokenSource _tokenSource = new CancellationTokenSource();

    private async void Run()
    {
        try
        {
            await TestA();
        }
        catch (OperationCanceledException e)
        {
            // handle the exception
            Debug.Log(e.Message);
        }
    }

    [Button()]
    public void CancleTestA()
    {
        _tokenSource.Cancel();
    }

    private Task TestA()
    {
        return Task.Run(() =>
        {
            for (int i = 0; i < 10000000; i++)
            {
                if (_tokenSource.Token.IsCancellationRequested)
                {
                    // clean up before exiting
                    _tokenSource.Token.ThrowIfCancellationRequested();
                    Debug.Log("ThrowIfCancellationRequested");
                }

                var example = Mathf.Pow(2, 10);
                // do long-running processing
            }

            Debug.Log("Done");
            return 42;
        }, _tokenSource.Token);
    }

    #endregion


    #region WaitAny, WaitAll, WhenAny, WhenAll

    /*  Task.WaitAll blocks the current thread until everything has completed.
        Task.WhenAll returns a task which represents the action of waiting until everything has completed.
        That means that from an async method, you can use:
        await Task.WhenAll(tasks);
        ... which means your method will continue when everything's completed, but you won't tie up a thread to just hang around until that time.
    */
    [Button()]
    public async void TestWaitAny()
    {
        var backgroundTasks = new[]
        {
            Task.Run(HeavyTaskAsyncA),
            Task.Run(HeavyTaskAsyncB),
            Task.Run(HeavyTaskAsyncC),
            Task.Run(HeavyTaskAsyncD)
        };
        Task.WaitAny(backgroundTasks);
        Debug.Log("Have one task compelte");
    }

    [Button()]
    public async void TestWaitAll()
    {
        var backgroundTasks = new[]
        {
            Task.Run(HeavyTaskAsyncA),
            Task.Run(HeavyTaskAsyncB),
            Task.Run(HeavyTaskAsyncC),
            Task.Run(HeavyTaskAsyncD)
        };
        Task.WaitAll(backgroundTasks);
        Debug.Log("All task compelte");
    }

    [Button()]
    public async void TestWhenAny()
    {
        var backgroundTasks = new[]
        {
            Task.Run(HeavyTaskAsyncA),
            Task.Run(HeavyTaskAsyncB),
            Task.Run(HeavyTaskAsyncC),
            Task.Run(HeavyTaskAsyncD)
        };
        await Task.WhenAny(backgroundTasks);
        Debug.Log("Have one task compelte");
    }

    [Button()]
    public async void TestWhenAll()
    {
        var backgroundTasks = new[]
        {
            Task.Run(HeavyTaskAsyncA),
            Task.Run(HeavyTaskAsyncB),
            Task.Run(HeavyTaskAsyncC),
            Task.Run(HeavyTaskAsyncD)
        };
        await Task.WhenAll(backgroundTasks);
        Debug.Log("All task compelte");
    }

    #endregion


    #region multiple tasks consecutively

    [Button()]
    public void RunMultipleTaskConsecutive()
    {
        var compositeTask = Task.Run(() => HeavyTaskAsyncE(100))
            .ContinueWith(previous => HeavyTaskAsyncF(previous.Result), TaskContinuationOptions.OnlyOnRanToCompletion);
    }


    /*
     *The ContinueWith() method allows you to chain multiple task to be executed one after another.
     * The continuing task gets a reference to the previous task to use its result or to check its status.
     * You can also add a condition to control when to run the continuation, e.g. only when the previous task completed successfully or when it threw an exception.
     * This adds flexibility in comparison to consecutively awaiting multiple tasks.
     * Of course, you can combine continuations with all the previously discussed features: exception handling,
     * cancellation and running tasks in parallel. This gives you a lot of expressive power to combine the tasks in different ways
     */
    [Button()]
    public async void RunMultipleTaskConsecutiveAdvance()
    {
        var multipleTasks = new[]
        {
            Task.Run(() => HeavyTaskAsyncE(1)),
            Task.Run(() => HeavyTaskAsyncE(2)),
            Task.Run(() => HeavyTaskAsyncE(3)),
            Task.Run(() => HeavyTaskAsyncE(4)),
        };
        var combinedTask = Task.WhenAll(multipleTasks);

        var successfulContinuation = combinedTask.ContinueWith(task => CombineResults(task.Result), TaskContinuationOptions.OnlyOnRanToCompletion);
        var failedContinuation = combinedTask.ContinueWith(task => HandleError(task.Exception), TaskContinuationOptions.NotOnRanToCompletion);

        Debug.Log("AAAAAAAA");
        await Task.WhenAny(successfulContinuation, failedContinuation);
        Debug.Log("DONE");
    }

    private void HandleError(AggregateException taskException)
    {
        Debug.Log(taskException.Message);
    }

    private void CombineResults(int[] taskResult)
    {
        Debug.Log(taskResult.Sum());
    }

    #endregion


    #region task synchronization

    /*If tasks are completely independent, the methods we just saw for coordinating them will suffice.
      However, as soon as they need to access shared data concurrently, additional synchronization is required in order to prevent data corruption.
      Whenever two or more threads attempt to modify a data structure in parallel, data can quickly become inconsistent.
      The following snippet of code is one such example:
      
      --------------------------------------CODE ERROR-----------------------------------
        var counters = new Dictionary< int, int >();
 
        if (counters.ContainsKey(key))
        {
            counters[key] ++;
        }
        else
        {
            counters[key] = 1;
        }
        
        
        When multiple threads execute the above code in parallel, a specific execution order of instructions in different threads can cause the data to be incorrect, e.g.:
            Both threads check the condition for the same key value when it is not yet present in the collection.
            As a result, they both enter the else block and set the value for this key to 1.
            Final counter value will be 1 instead of 2, which would be the expected result if the threads would execute the same code consecutively.
      -----------------------------------------------------------------------------------
      
      
      *Such blocks of code, which may only be entered by one thread at a time, are called critical sections.
      *In C#, you can protect them by using the lock statement:
     * -------------------------------------CODE RIGHT------------------------------------
        var counters = new Dictionary< int, int >();
 
        lock (syncObject)
        {
            if (counters.ContainsKey(key))
            {
                counters[key]++;
            }
            else
            {
                counters[key] = 1;
            }
        }
      
        For this approach to work, all threads must share the same syncObject as well. As a best practice, 
        syncObject should be a private Object instance that is exclusively used for protecting access to a single critical section and cannot be accessed from outside.
        The lock statement will allow only one thread to access the block of code inside it. 
        It will block the next thread trying to access it until the previous one exits it. 
        This will ensure that a thread will execute the complete critical section of code without interruptions by another thread. Of course, 
        this will reduce the parallelism and slow down the overall execution of code, 
        therefore you will want to minimize the number of critical sections and to make them as short as possible
      -------------------------------------------------------------------------------------
      
      
        The lock statement is just a shorthand for using the Monitor class:

        var lockWasTaken = false;
        var temp = syncObject;
        try
        {
            Monitor.Enter(temp, ref lockWasTaken);
            // lock statement body
        }
        finally
        {
            if (lockWasTaken)
            {
                Monitor.Exit(temp);
            }
        }
        
        Although most of the time you will want to use the lock statement, 
        Monitor class can give you additional control when you need it. 
        For example, you can use TryEnter() instead of Enter() and specify a timeout to avoid waiting indefinitely for the lock to release.
        
    */

    private Dictionary<int, int> _counters = new Dictionary<int, int>();

    private async void TaskNoneSynchronization1(int key)
    {
        await Task.Run(() =>
        {
            if (_counters.ContainsKey(key))
            {
                _counters[key]++;

                Debug.Log($"[A] TaskNoneSynchronization1 Change [{key}] :" + _counters[key]);
            }
            else
            {
                _counters[key] = 1;
                Debug.Log($"[B] TaskNoneSynchronization1 Change [{key}] :" + _counters[key]);
            }

            Debug.Log("Done Task non synchronization ");
        });
    }


    [Button()]
    public async void TestNonSynchronization()
    {
        _counters.Add(1, 0);
        _counters.Add(2, 0);


        var tasks = new[]
        {
            Task.Run(() => TaskNoneSynchronization1(1)),
            Task.Run(() => TaskNoneSynchronization1(3)),
            Task.Run(() => TaskNoneSynchronization1(3)),
            Task.Run(() => TaskNoneSynchronization1(3)),
            Task.Run(() => TaskNoneSynchronization1(3)),
        };

        await Task.WhenAll(tasks);
        foreach (var item in _counters.Keys)
        {
            Debug.Log(item + " == " + _counters[item]);
        }
    }


    [Button()]
    public async void TestSynchronization()
    {
        _counters.Clear();
        _counters.Add(1, 0);
        _counters.Add(2, 0);

        var tasks = new[]
        {
            Task.Run(() => TaskSynchronization1(1)),
            Task.Run(() => TaskSynchronization1(3)),
            Task.Run(() => TaskSynchronization1(3)),
            Task.Run(() => TaskSynchronization1(3)),
            Task.Run(() => TaskSynchronization1(3)),
        };

        var temp = Task.WhenAll(tasks);
        await temp.ContinueWith(task => { }, TaskContinuationOptions.OnlyOnRanToCompletion);

        Debug.Log("HAHAHA");
        foreach (var item in _counters.Keys.ToList())
        {
            Debug.Log(item + " == " + _counters[item]);
        }
    }

    private async void TaskSynchronization1(int key)
    {
        await Task.Run(() =>
        {
            lock (this)
            {
                if (_counters.ContainsKey(key))
                {
                    _counters[key]++;

                    Debug.Log($"[A] TaskSynchronization1 Change [{key}] :" + _counters[key]);
                }
                else
                {
                    _counters.Add(key, 1);
                    Debug.Log($"[B] TaskSynchronization1 Change [{key}] :" + _counters[key]);
                }

                Debug.Log("Done Task Synchronization : " + key);
            }
        });
    }

    #endregion


    #region concurrent collections

    //ex : ConcurrentDictionary, ImmutableDictionary ...

    /*
     *
     *When a critical section is required only to ensure atomic access to a data structure,
     * a specialized data structure for concurrent access might be a better and more performant alternative.
     * For example, by using ConcurrentDictionary instead of Dictionary, the lock statement example can be simplified:


       // var counters = new ConcurrentDictionary< int, int >();
 
       // counters.TryAdd(key, 0);
       // lock (syncObject)
       // {
       //     counters[key]++;
       // }
        
        
        
       *Naively, one might even want to use the following:

       // counters.AddOrUpdate(key, 1, (oldKey, oldValue) => oldValue + 1);
     
     
        However, the update delegate in the above method is executed outside the critical section.
        Therefore a second thread could still read the same old value as the first thread, before the first one has updated it, 
        effectively overwriting the first thread’s update with its own value and losing one increment. 
        Even concurrent collections are not immune to multithreading issues when used incorrectly.

        Another alternative to concurrent collections, is immutable collections.

        Similar to concurrent collections they are also thread safe, but the underlying implementation is different.
        Any operations that change the data structures do not modify the original instance.
        Instead, they return a changed copy and leave the original instance unchanged:

        // var original = new Dictionary< int, int >().ToImmutableDictionary();
        // var modified = original.Add(key, value);
        
        
        Because of this, any changes to the collection in one thread are not visible to the other threads,
        as they still reference the original unmodified collection, which is the very reason why immutable collections are inherently thread safe.

        Of course, this makes them useful for a different set of problems. 
        They work best in cases, when multiple threads require the same input collection and then modify it independently, 
        potentially with a final common step that merges the changes from all the threads. With regular collections, 
        this would require creating a copy of the collection for each thread in advance.
     * 
     */

    #endregion


    #region Parallel LINQ

    public void CreateParalelExample()
    {
        //Cách dùng for bình thường ta vẫn hay dùng
        for (var i = 0; i < 100; i++)
        {
            // TODO
        }

        //Cách dùng với lớp parallel
        Parallel.For(0, 100, i =>
        {
            // TODO
        });

        //Hoặcvới một IEnumerable

        IEnumerable<GameObject> myEnumerable = new List<GameObject>();

        Parallel.ForEach(myEnumerable, obj =>
        {
            // TODO
        });
    }


    /*
 
    IEnumerable<MyObject> source = ...
 
    // LINQ
    var query1 = from i in source select Normalize(i);
 
    // PLINQ = parallel LINQ
    var query2 = from i in source.AsParallel()
        select Normalize(i);
        
        
    //Hoặc
    IEnumerable<MyObject> myEnumerable = ...
 
    myEnumerable.AsParallel().ForAll(obj => DoWork(obj));
    
    */


    [Button()]
    public void CreateSequential()
    {
        Stopwatch watch = new Stopwatch();
        watch.Start();
        // sequential execution
        var sequential = Enumerable.Range(0, 4000000)
            .Select(ExpensiveOperation)
            .ToArray();

        watch.Stop();
        Debug.Log(watch.ElapsedMilliseconds);
    }


    [Button()]
    public void CreateParallel()
    {
        Stopwatch watch = new Stopwatch();
        watch.Start();
        // parallel execution
        var parallel = Enumerable.Range(0, 4000000)
            .AsParallel()
            .Select(ExpensiveOperation)
            .ToArray();

        watch.Stop();
        Debug.Log(watch.ElapsedMilliseconds);
    }

    private static int ExpensiveOperation(int i)
    {
        var pow = Math.Pow(2, 10);
        return i;
    }


    /*
     * Như trường hợp trên Parallel sẽ thực thi chậm hơn so với thông thường nếu như biểu thức tính toán trong ExpensiveOperation không liên quan đến CPU hoặc là các phép tính đơn giản.
     *
     *
     * -------------------------------------------  Parallel lợi hơn -----------------------------
     * Một đám ruộng cần phải gặt xong càng sớm càng tốt,
     * chúng ta có 5 người( tức Parallel.For chạy từ 1 – 5) ,
     * mỗi người sẽ gặt 1/5 đám ruộng,
     * ai song trước thì nghỉ( là Parallel.For/ForEach) .
     *
     * Y vậy, nếu việc gặt lúa song song cho ta lợi 3t về time và việc chia phần đám ruộng cho 5 người mất 1t
     * thì chúng ta vẫn lợi 2t về thời gian hơn so với việc 5 người lần lược thay nhau gặt ( là for/foreach). => Parallel.* lợi hơn
     * 
     * Lưu ý rằng trong trường hợp trên nhưng time bị mất cho việc chia phần công việc lớn hơn nhiều so với tổng chi phí công việc phải làm thì Parallel không phát huy hiệu quả =>  rất chậm.
     * Chia phần công việc được hiểu là việc hệ thống sẽ tiến hành phân tích và chia Thread chia Core, tính toán đủ thứ.
     *
     *
     *
     * -----------------------------------------  Không nên dùng parallel -------------------------
     * Một đám ruộng cần phải gặt xong càng sớm càng tốt,
     * chúng ta có 5 người( tức Parallel.For chạy từ 1 – 5) ,
     * vấn đề ở đây là 5 người nhưng chỉ có 1 chiếc “Liềm” nên phải share cho nhau.
     * Vậy cuối cùng cũng chỉ có duy nhất một người gặt tại một thời điểm => chẳng khác gì thay nhau gặt;
     * cộng thêm thời gian chia phần đám ruộng cho 5 người trước khi gặt
     * thì ta mất nhiều thời gian hơn(vi nếu bạn chia phần công việc trong code hay trước khi run ứng dụng thì lúc run-time CPU sẽ không cần thực thi việc phân chia core/thread)
     *    => for.foreach lợi hơn
     * 
     */

    private static int Fibonacci(int x)
    {
        if (x <= 1)
        {
            return 1;
        }

        return Fibonacci(x - 1) + Fibonacci(x - 2);
    }

    private static void DummyWork()
    {
        for (var i = 0; i < 100; i++)
        {
            var example = Math.Pow(2, 10);
        }
    }

    private const int TOTAL_WORK_ITEMS = 2000000;

    private static void SerialWork(int outerWorkItems)
    {
        int innerLoopLimit = TOTAL_WORK_ITEMS / outerWorkItems;
        for (int index1 = 0; index1 < outerWorkItems; index1++)
        {
            InnerLoop(innerLoopLimit);
        }
    }

    private static void InnerLoop(int innerLoopLimit)
    {
        for (int index2 = 0; index2 < innerLoopLimit; index2++)
        {
            DummyWork();
        }
    }

    private static void ParallelWork(int outerWorkItems)
    {
        int innerLoopLimit = TOTAL_WORK_ITEMS / outerWorkItems;
        var outerRange = Enumerable.Range(0, outerWorkItems);
        Parallel.ForEach(outerRange, index1 => { InnerLoop(innerLoopLimit); });
    }

    private static void TimeOperation(string desc, Action operation)
    {
        Stopwatch timer = new Stopwatch();
        timer.Start();
        operation();
        timer.Stop();

        var message = $"{desc} took {timer.ElapsedMilliseconds}";
        Debug.Log(message);
    }

    [Button()]
    public void TestParallel()
    {
        TimeOperation("serial work: 1", () => SerialWork(1));
        TimeOperation("serial work: 2", () => SerialWork(2));
        TimeOperation("serial work: 3", () => SerialWork(3));
        TimeOperation("serial work: 4", () => SerialWork(4));
        TimeOperation("serial work: 8", () => SerialWork(8));
        TimeOperation("serial work: 16", () => SerialWork(16));
        TimeOperation("serial work: 32", () => SerialWork(32));
        TimeOperation("serial work: 1k", () => SerialWork(1000));
        TimeOperation("serial work: 10k", () => SerialWork(10000));
        TimeOperation("serial work: 100k", () => SerialWork(100000));

        TimeOperation("parallel work: 1", () => ParallelWork(1));
        TimeOperation("parallel work: 2", () => ParallelWork(2));
        TimeOperation("parallel work: 3", () => ParallelWork(3));
        TimeOperation("parallel work: 4", () => ParallelWork(4));
        TimeOperation("parallel work: 8", () => ParallelWork(8));
        TimeOperation("parallel work: 16", () => ParallelWork(16));
        TimeOperation("parallel work: 32", () => ParallelWork(32));
        TimeOperation("parallel work: 64", () => ParallelWork(64));
        TimeOperation("parallel work: 1k", () => ParallelWork(1000));
        TimeOperation("parallel work: 10k", () => ParallelWork(10000));
        TimeOperation("parallel work: 100k", () => ParallelWork(100000));
    }

    #endregion


    private async Task HeavyTaskAsync()
    {
        await Task.Run(() =>
        {
            for (int i = 0; i < 10000; i++)
            {
                var example = Mathf.Pow(2, 10);
            }
        });
    }

    private async Task HeavyTaskAsyncA()
    {
        await Task.Run(() =>
        {
            for (int i = 0; i < 100000000; i++)
            {
                var example = Mathf.Pow(2, 10);
            }

            Debug.Log("Done A");
        });
    }

    private async Task HeavyTaskAsyncB()
    {
        await Task.Run(() =>
        {
            for (int i = 0; i < 8000000; i++)
            {
                var example = Mathf.Pow(2, 10);
            }

            Debug.Log("Done B");
        });
    }

    private async Task HeavyTaskAsyncC()
    {
        await Task.Run(() =>
        {
            for (int i = 0; i < 100000000; i++)
            {
                var example = Mathf.Pow(2, 10);
            }

            Debug.Log("Done C");
        });
    }

    private async Task HeavyTaskAsyncD()
    {
        await Task.Run(() =>
        {
            for (int i = 0; i < 10000000; i++)
            {
                var example = Mathf.Pow(2, 10);
            }

            Debug.Log("Done D");
        });
    }

    private Task<int> HeavyTaskAsyncE(int value)
    {
        return Task.Run(() =>
        {
            for (int i = 0; i < 500000; i++)
            {
                var example = Mathf.Pow(2, 10);
            }

            Debug.Log("Done Task 1 :" + value);
            return value * 2;
        });
    }

    private async void HeavyTaskAsyncF(int value)
    {
        await Task.Run(() =>
        {
            for (int i = 0; i < 50000; i++)
            {
                var example = Mathf.Pow(2, 10);
            }

            Debug.Log("Done Task 2 ");
            Debug.Log("result : " + value);
        });
    }
}

```


## License

Copyright (c) 2019 yenmoc

Released under the MIT license, see [LICENSE.txt](LICENSE.txt)

